import React from 'react';
import './App.css';
import 'bootstrap/dist/css/bootstrap.min.css';

function App() {
  let salutClassName = "mouseOut";
  
  return (
    <div className="App">
      <div className="conteiner-fluid general">
        <div className="row ligne">
          <div className="col-md-5 col-lg-5 col-12 order-2 order-md-1 order-lg-1 colon1 text-md-right text-lg-right text-center">
            <a href="#"
              className="homeLinks"
              onMouseEnter={() => salutClassName = "mouseEnter"}
              onMouseLeave={() => salutClassName = "mouseOut"}>
                <h1 className={salutClassName} id="decalageMenu">Salut.</h1>
            </a>
            <a href="#"
                className="homeLinks">
              <h1 className="israilStyle">Je suis</h1>
            </a>
            <a href="#" className="homeLinks"><h1 className="israilStyle">Israil</h1></a>
          </div>
          <div className="col-md-7 col-lg-7 col-12 order-1 order-md-2 order-lg-2 colon2">
            <img id="israilImg" src='imageIsrail.svg'/>
          </div>
        </div>
      </div>
    </div>
  );
}
  
export default App;